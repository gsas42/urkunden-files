// Setup indexes
CREATE INDEX urkunde_index IF NOT EXISTS FOR (u:Urkunde) ON (u.id);
CREATE INDEX region_index IF NOT EXISTS FOR (r:Region) On (r.label);
CREATE INDEX archiv_index IF NOT EXISTS FOR (a:Archiv) On (a.label);
CREATE INDEX sach_index IF NOT EXISTS FOR (s:Sachbetreff) On (s.label);

// Empty the graph
MATCH (n) DETACH DELETE n;

// Import Thesaurus data transformed to CSV files
LOAD CSV WITH HEADERS FROM 'https://git.thm.de/jbrg89/urkunden-files/-/raw/master/regional-thesaurus.csv' as row FIELDTERMINATOR ';'
WITH row
MERGE (r:Region {label: row.place})
WITH row, r
WHERE row.sub_of IS NOT null
MATCH (r1:Region {label: row.sub_of})
MERGE (r)-[:SUB_OF]->(r1);

LOAD CSV WITH HEADERS FROM 'https://git.thm.de/jbrg89/urkunden-files/-/raw/master/archiv-thesaurus.csv' as row1 FIELDTERMINATOR ';'
WITH row1
MERGE (a:Archiv {label: row1.place})
WITH row1, a
WHERE row1.sub_of IS NOT null
MATCH (a1:Archiv {label: row1.sub_of})
MERGE (a)-[:SUB_OF]->(a1);

LOAD CSV WITH HEADERS FROM 'https://git.thm.de/jbrg89/urkunden-files/-/raw/master/sach-thesaurus.csv' as row2 FIELDTERMINATOR ';'
WITH row2
MERGE (s:Sachbetreff {label: row2.place})
WITH row2, s
WHERE row2.sub_of IS NOT null
MATCH (s1:Sachbetreff {label: row2.sub_of})
MERGE (s)-[:SUB_OF]->(s1);

// Separate runs are performed for each node type to filter with WITH x IS NOT NULL

// Create the 'Urkunden' nodes
:auto LOAD CSV WITH HEADERS FROM 'https://git.thm.de/aksz15/data/-/raw/master/f3Urkunden/f3Urkunden.csv' AS row
CALL {
    WITH row
    MERGE (u:Urkunde {id: row.Objektnummer})
    ON CREATE SET
    u.quellenDatum=row.Quellendatum,
    u.sortierDatum=row.SortierDatum,
    u.urkundenNummer=toInteger(row.UrkundenNummer),
    u.uNummer=row.UNummer,
    u.kVr=row.KVr,
    u.kVv=row.KVv,
    u.kVRPosition=row.KVRPosition,
    u.kVVPosition=row.KVVPosition,
    u.beschreibstoff=row.Beschreibstoff,
    u.diplomatische_Form=row.Diplomatische_Form,
    u.chmelbeleg=row.Chmelbeleg,
    u.chmelNr=row.ChmelNr,
    u.chmelRegg=row.ChmelRegg,
    u.chmelText=row.ChmelText,
    u.taxreg=split(coalesce(row.Taxreg, ""), ";"),
    u.druckbeleg=row.Druckbeleg,
    u.rFIII=split(coalesce(row.RFIII, ""), ";"),
    u.regestbeleg=split(coalesce(row.Regestbeleg, ""), ";"),
    u.erwähnung=row.Erwähnung,
    u.literatur=row.Literatur,
    u.foto=row.Foto,
    u.pön=row.Pön,
    u.wasserzeichen=row.Wasserzeichen,
    u.nachtragsfelder=row.Nachtragsfelder,
    u.bemerkungen=split(coalesce(row.Bemerkungen, ""), ";"),
    u.sonstiges=row.Sonstiges,
    u.urkNummer=toInteger(row.UrkNummer),
    u.rfNr=row.RfNr,
    u.ergibt_sich_aus=split(coalesce(row.Ergibt_sich_aus, ""), ";"),
    u.indexDatum=row.IndexDatum,
    u.sF=toInteger(row.SF),
    u.editionsDatum=row.EditionsDatum,
    u.kurzregest=row.Kurzregest,
    u.reggF=row.ReggF,
    u.weitere_Nachweise=split(coalesce(row.Weitere_Nachweise, ""), ";"),
    u.mehrfachüberlieferung_von=row.Mehrfachüberlieferung_von,
    u.rR=row.RR,
    u.orte=split(coalesce(row.Orte, ""), ";"),
    u.personen=split(coalesce(row.Personen, ""), ";"),
    u.urkunde=row.Urkunde,
    u.objektart=row.Objektart,
    u.angaben_zum_Original=row.Angaben_zum_Original,
    u.anmerkungen_zur_Kopie=row.Anmerkungen_zur_Kopie,
    u.art_der_Kopie=row.Art_der_Kopie,
    u.datierungszeile_der_Kopie=row.Datierungszeile_der_Kopie,
    u.datum_der_Kopie=row.Datum_der_Kopie,
    u.ausstellungsort=row.Ausstellungsort,
    u.besiegelung_der_Kopie=row.Besiegelung_der_Kopie,
    u.iDNummer=row.IDNummer,
    u.kopie=row.Kopie,
    u.kopieStoff=row.KopieStoff,
    u.kV_Anmerkungen=row.KV_Anmerkungen,
    u.kVrecto=row.KVrecto,
    u.maske=row.Maske,
    u.reggFApparat=row.ReggFApparat,
    u.reggFKV=row.ReggFKV,
    u.reggFText=row.ReggFText,
    u.reggFÜberlieferung=row.ReggFÜberlieferung,
    u.registerText=row.RegisterText,
    u.registrator=row.Registrator,
    u.registraturvermerk=row.Registraturvermerk,
    u.rF=row.RF,
    u.weitere_Überlieferung=row.Weitere_Überlieferung
} IN TRANSACTIONS;

// Create the 'Siegel' nodes and the relationship to 'Urkunden' for the Siegel
:auto LOAD CSV WITH HEADERS FROM 'https://git.thm.de/aksz15/data/-/raw/master/f3Urkunden/f3Urkunden.csv' AS row
CALL {
    WITH row
    MATCH (u:Urkunde {id: row.Objektnummer})
    WITH row, u WHERE row.Siegel_PosseNr IS NOT NULL
    MERGE (sp:SiegelPosse {posseNr: row.Siegel_PosseNr})
    CREATE (s:Siegel {
        istSekretsiegel: false,
        siegelfarbe: row.Siegelfarbe,
        anbringung: row.Anbringung,
        schnurfarbe: row.Schnurfarbe
    })
    MERGE (u)-[:HAT_SIEGEL]->(s)
    MERGE (s)-[:VON_POSSE]-(sp)
} IN TRANSACTIONS;

// Create the 'Siegel' nodes and the relationship to 'Urkunden' for the Sekretsiegel
:auto LOAD CSV WITH HEADERS FROM 'https://git.thm.de/aksz15/data/-/raw/master/f3Urkunden/f3Urkunden.csv' AS row
CALL {
    WITH row
    MATCH (u:Urkunde {id: row.Objektnummer})
    WITH row, u WHERE row.Sekretsiegel_PosseNr IS NOT NULL
    MERGE (sp:SiegelPosse {posseNr: row.Sekretsiegel_PosseNr})
    CREATE (s:Siegel {
        istSekretsiegel: true,
        siegelfarbe: row.Sekretsiegelfarbe,
        lage: row.Sekretsiegellage
    })
    MERGE (u)-[:HAT_SIEGEL]->(s)
    MERGE (s)-[:VON_POSSE]-(sp)
} IN TRANSACTIONS;

// Create the 'Kanzlist' nodes and the relationship to 'Urkunden'
:auto LOAD CSV WITH HEADERS FROM 'https://git.thm.de/aksz15/data/-/raw/master/f3Urkunden/f3Urkunden.csv' AS row
CALL {
    WITH row
    MATCH (u:Urkunde {id: row.Objektnummer})
    WITH u, row WHERE row.Kanzlist IS NOT NULL
    MERGE (k:Kanzlist {name: row.Kanzlist})
    SET k.amtsbezeichnung=row.Amtsbezeichnung
    WITH k, u, row
    MERGE (u)<-[:ERSTELLT_VERMERK]-(k)
} IN TRANSACTIONS;

// Create the 'Referent' nodes and the relationship to 'Urkunden'
:auto LOAD CSV WITH HEADERS FROM 'https://git.thm.de/aksz15/data/-/raw/master/f3Urkunden/f3Urkunden.csv' AS row
CALL {
    WITH row
    MATCH (u:Urkunde {id: row.Objektnummer})
    WITH u, row WHERE row.Referent IS NOT NULL
    MERGE (r:Referent {name: row.Referent})
    WITH r, u, row
    MERGE (u)<-[:REFERENT_VERMERKT]-(r)
} IN TRANSACTIONS;

// Create the 'Empfaenger' nodes and the relationship to 'Urkunden'
:auto LOAD CSV WITH HEADERS FROM 'https://git.thm.de/aksz15/data/-/raw/master/f3Urkunden/f3Urkunden.csv' AS row
CALL {
    WITH row
    MATCH (u:Urkunde {id: row.Objektnummer})
    WITH u, row WHERE row.Empfaenger IS NOT NULL
    MERGE (em:Empfaenger {name: row.Empfänger})
    WITH u, em, row
    MERGE (u)-[:EMPFÄNGT]->(em)
} IN TRANSACTIONS;

// Create the 'Korrektor' nodes and the relationship to 'Urkunden'
:auto LOAD CSV WITH HEADERS FROM 'https://git.thm.de/aksz15/data/-/raw/master/f3Urkunden/f3Urkunden.csv' AS row
CALL {
    WITH row 
    MATCH (u:Urkunde {id: row.Objektnummer})
    WITH u, row WHERE row.Korrigiert_von IS NOT NULL
    MERGE (k:Korrektor {name: row.Korrigiert_von})
    WITH u, k, row
    MERGE (u)-[ko:KORRIGIERT_VON]->(k)
    ON CREATE SET ko.korrektur=row.Korrektur
} IN TRANSACTIONS;

// Create the 'Erfasser' nodes and the relationship to 'Urkunden'
:auto LOAD CSV WITH HEADERS FROM 'https://git.thm.de/aksz15/data/-/raw/master/f3Urkunden/f3Urkunden.csv' AS row
CALL {
    WITH row
    MATCH (u:Urkunde {id: row.Objektnummer})
    WITH u, row WHERE row.Erfasser IS NOT NULL
    MERGE (er:Erfasser {name: row.Erfasser})
    WITH u, er, row
    MERGE (u)-[ev:ERFASST_VON]->(er)
    ON CREATE SET ev.zugang=row.Zugang
} IN TRANSACTIONS;

// Connect the 'Urkunden' nodes with the 'Sachbetreff' nodes from the Thesaurus data
:auto LOAD CSV WITH HEADERS FROM 'https://git.thm.de/aksz15/data/-/raw/master/f3Urkunden/f3Urkunden.csv' AS row
CALL {
    WITH row
    MATCH (u:Urkunde {id: row.Objektnummer})
    WITH u, row WHERE row.Sachbetreff IS NOT NULL
    MATCH (sa:Sachbetreff)
    WHERE sa.label=row.Sachbetreff
    MERGE (sa)<-[:HAT_SACHBETREFF]-(u)
} IN TRANSACTIONS;

// Connect the 'Urkunden' nodes with the 'Archiv' nodes from the Thesaurus data
:auto LOAD CSV WITH HEADERS FROM 'https://git.thm.de/aksz15/data/-/raw/master/f3Urkunden/f3Urkunden.csv' AS row
CALL {
    WITH row
    MATCH (u:Urkunde {id: row.Objektnummer})
    WITH u, row WHERE row.Archiv IS NOT NULL
    MATCH (arch:Archiv)
    WHERE arch.label=row.Archiv
    MERGE (u)-[nachw:NACHWEIS_IN]->(arch)
    SET nachw.signatur=row.Signatur
} IN TRANSACTIONS;

// Connect the 'Urkunden' nodes with the 'Region' nodes from the Thesaurus data
:auto LOAD CSV WITH HEADERS FROM 'https://git.thm.de/aksz15/data/-/raw/master/f3Urkunden/f3Urkunden.csv' AS row
CALL {
    WITH row
    MATCH (u:Urkunde {id: row.Objektnummer})
    WITH u, row WHERE row.Region IS NOT NULL
    MATCH (reg:Region)
    WHERE reg.label=row.Region
    MERGE (u)-[:ERSCHLOSSEN_IN]->(reg)
} IN TRANSACTIONS;

// rename the 'Region' label to 'Ort' label
MATCH (r:Region)
REMOVE r:Region
SET r:Ort

// add AUSGESTELLT_IN relationship between 'Urkunden' nodes and 'Ort' nodes
MATCH(u:Urkunde)
WHERE u.ausstellungsort IS NOT NULL
MERGE (r:Ort { label: u.ausstellungsort })
MERGE (u)-[:AUSGESTELLT_IN]-(r)
SET u.ausstellungsort = null

// add ERWAEHNT relationship between 'Urkunden' nodes and 'Ort' nodes
MATCH(u:Urkunde)
WHERE size(u.orte) > 0
WITH u.orte as orte, u
FOREACH(ort in orte | 
MERGE (r:Ort { label: ort})
MERGE (u)-[:ERWAEHNT]->(r)
)
SET u.orte = null
