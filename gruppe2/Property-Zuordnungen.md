# Liste der Property-Zuordnungen
Dieses Dokument zeigt auf, wie die einzelnen Header der CSV-Datei den Knoten zugeordnet wurden. Wenn die Daten eines Felds nach dem Import überprüft wurden, ist dies entsprechend eingetragen.

## Urkunde
Objektart -> OK
Angaben_zum_Original -> OK
Anmerkungen_zur_Kopie -> OK
Art_der_Kopie -> OK
Datierungszeile_der_Kopie -> OK
Datum_der_Kopie -> OK
Kurzregest -> OK
ReggF -> OK
RR -> OK
Nachtragsfelder -> OK
Bemerkungen -> OK (Liste mit ;)
Sonstiges -> OK
UrkNummer -> OK (Transformation zu Int)
RfNr -> OK
Erwähnung -> OK
Literatur -> OK
Foto -> ?? (null)
Pön -> OK
Druckbeleg -> OK
Regestbeleg -> OK (Liste mit ;)
KVv -> OK
Chmelbeleg -> OK
Quellendatum -> OK
UrkundenNummer -> OK (Transformation zu Int)
KVr -> OK (z.B. "A.m.d.r. Hermannus Hecht")
Objektnummer -> OK
Personen -> OK (Liste mit ;)
IndexDatum -> OK (ggf. transformieren?)
Orte -> OK (Liste mit ;)
Weitere_Nachweise -> OK (Liste mit ;)
KVRPosition -> OK
RFIII -> OK (Liste mit ;)
Taxreg -> OK (Liste mit ;)
Wasserzeichen -> OK
Ergibt_sich_aus -> OK (Liste mit ;)
EditionsDatum -> OK
SF -> OK (Transformation zu Int)
Urkunde -> OK
Beschreibstoff -> OK
Weitere_Überlieferung -> OK
Foto -> OK
KV_Anmerkungen -> OK
KVrecto -> OK
SortierDatum -> OK (Transformation nötig?)
Besiegelung_der_Kopie -> OK

### Anpassungen sinnvoll
KVVPosition -> OK (scheinbar irgendeine Positionierung)
Diplomatische_Form -> OK (ggf. Refactor in Knoten)
ChmelRegg -> OK (wiederholte Werte)
Ausstellungsort -> OK (Refaktorisierung in Label denkbar)
Registrator -> OK (Refactor als Knoten denkbar)
Registraturvermerk -> OK (wiederholende Werte)

### Immer null
UNummer -> (immer null)
ChmelNr -> (immer null)
ChmelText -> (immer null)
Mehrfachüberlieferung_von -> (immer null)
IDNummer -> (immer null)
Kopie -> (immer null)
KopieStoff -> (immer null)
Maske -> (immer null)
ReggFApparat -> (immer null)
ReggFKV -> (immer null)
ReggFText -> (immer null)
ReggFÜberlieferung -> (immer null)
RegisterText -> (immer null)
RF -> (immer null)

## Siegel
Sekretsiegel_PosseNr -> (immer null)
Sekretsiegelfarbe -> (immer null)
Siegel_PosseNr (ID) -> OK
Siegelfarbe -> (immer null)
### HAT_SIEGEL
Anbringung -> OK
Schnurfarbe -> OK
Sekretsiegellage -> OK
Besiegelung_Anmerkungen -> OK

## Region
Region

## Sachbetreff
Sachbetreff -> OK

## Erfasser
Erfasser -> OK
### ERFASST_VON
Zugang -> OK

## Korrektor
Korrigiert_von (ID) -> OK
### KORRIGIERT_VON
Korrektur -> OK

## Empfänger
Empfänger (ID)

## Archiv
Archiv -> OK
### NACHWEIS_IN
Signatur

## Referent
Referent -> OK

## Kanzlist
Kanzlist (ID) -> OK
Amtsbezeichnung -> OK

### Ortsbetreff
Ortsangaben
Ortsname
Ortsname_der_Quelle

### Biographie
Biographisches
Familienname
Personenname
Lebensdaten

# Anmerkungen:
Kopie-Infos von Kopial einfach bei Urkunden reingenommen, Kopial könnte eigener Typ
- Datenfeld Korrigiert_von (falches Feld von Heinicker)