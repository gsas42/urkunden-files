:auto LOAD CSV WITH HEADERS FROM 'https://git.thm.de/aksz15/data/-/raw/master/f3Urkunden/f3Urkunden.csv' AS row
CALL {
    WITH row
    WITH row LIMIT 100
    MERGE (u:Urkunde {id: row.Objektnummer})
    ON CREATE SET
    u.quellenDatum=row.Quellendatum,
    u.sortierDatum=row.SortierDatum,
    u.urkundenNummer=toInteger(row.UrkundenNummer),
    u.uNummer=row.UNummer,
    u.kVr=row.KVr,
    u.kVv=row.KVv,
    u.kVRPosition=row.KVRPosition,
    u.kVVPosition=row.KVVPosition,
    u.beschreibstoff=row.Beschreibstoff,
    u.diplomatische_Form=row.Diplomatische_Form,
    u.chmelbeleg=row.Chmelbeleg,
    u.chmelNr=row.ChmelNr,
    u.chmelRegg=row.ChmelRegg,
    u.chmelText=row.ChmelText,
    u.taxreg=split(coalesce(row.Taxreg, ""), ";"),
    u.druckbeleg=row.Druckbeleg,
    u.rFIII=split(coalesce(row.RFIII, ""), ";"),
    u.regestbeleg=split(coalesce(row.Regestbeleg, ""), ";"),
    u.erwähnung=row.Erwähnung,
    u.literatur=row.Literatur,
    u.foto=row.Foto,
    u.pön=row.Pön,
    u.wasserzeichen=row.Wasserzeichen,
    u.nachtragsfelder=row.Nachtragsfelder,
    u.bemerkungen=split(coalesce(row.Bemerkungen, ""), ";"),
    u.sonstiges=row.Sonstiges,
    u.urkNummer=toInteger(row.UrkNummer),
    u.rfNr=row.RfNr,
    u.ergibt_sich_aus=split(coalesce(row.Ergibt_sich_aus, ""), ";"),
    u.indexDatum=row.IndexDatum,
    u.sF=toInteger(row.SF),
    u.editionsDatum=row.EditionsDatum,
    u.kurzregest=row.Kurzregest,
    u.reggF=row.ReggF,
    u.weitere_Nachweise=split(coalesce(row.Weitere_Nachweise, ""), ";"),
    u.mehrfachüberlieferung_von=row.Mehrfachüberlieferung_von,
    u.rR=row.RR,
    u.orte=split(coalesce(row.Orte, ""), ";"),
    u.personen=split(coalesce(row.Personen, ""), ";"),
    u.urkunde=row.Urkunde,
    u.objektart=row.Objektart,
    u.angaben_zum_Original=row.Angaben_zum_Original,
    u.anmerkungen_zur_Kopie=row.Anmerkungen_zur_Kopie,
    u.art_der_Kopie=row.Art_der_Kopie,
    u.datierungszeile_der_Kopie=row.Datierungszeile_der_Kopie,
    u.datum_der_Kopie=row.Datum_der_Kopie,
    u.ausstellungsort=row.Ausstellungsort,
    u.besiegelung_der_Kopie=row.Besiegelung_der_Kopie,
    u.iDNummer=row.IDNummer,
    u.kopie=row.Kopie,
    u.kopieStoff=row.KopieStoff,
    u.kV_Anmerkungen=row.KV_Anmerkungen,
    u.kVrecto=row.KVrecto,
    u.maske=row.Maske,
    u.reggFApparat=row.ReggFApparat,
    u.reggFKV=row.ReggFKV,
    u.reggFText=row.ReggFText,
    u.reggFÜberlieferung=row.ReggFÜberlieferung,
    u.registerText=row.RegisterText,
    u.registrator=row.Registrator,
    u.registraturvermerk=row.Registraturvermerk,
    u.rF=row.RF,
    u.weitere_Überlieferung=row.Weitere_Überlieferung
    WITH row, u WHERE row.Siegel_PosseNr IS NOT NULL
    MERGE (s:Siegel {siegel_PosseNr: row.Siegel_PosseNr})
    ON CREATE SET
    u.sekretsiegel_PosseNr=row.Sekretsiegel_PosseNr,
    u.sekretsiegelfarbe=row.Sekretsiegelfarbe,
    u.siegelfarbe=row.Siegelfarbe
    WITH u, s, row
    MERGE (u)-[h:HAT_SIEGEL]->(s)
    ON CREATE SET
    h.anbringung=row.Anbringung,
    h.schnurfarbe=row.Schnurfarbe,
    h.sekretsiegellage=row.Sekretsiegellage,
    h.besiegelung_Anmerkungen=row.Besiegelung_Anmerkungen
    WITH u, row WHERE row.Kanzlist IS NOT NULL
    MERGE (k:Kanzlist {name: row.Kanzlist})
    SET k.amtsbezeichnung=row.Amtsbezeichnung
    WITH k, u, row
    MERGE (u)<-[:ERSTELLT_VERMERK]-(k)
    WITH u, row WHERE row.Referent IS NOT NULL
    MERGE (r:Referent {name: row.Referent})
    WITH r, u, row
    MERGE (u)<-[:REFERENT_VERMERKT]-(r)
    WITH u, row WHERE row.Empfänger IS NOT NULL
    MERGE (em:Empfänger {name: row.Empfänger})
    WITH u, em, row
    MERGE (u)<-[:EMPFÄNGT]-(em)
    WITH u, row WHERE row.Korrigiert_von IS NOT NULL
    MERGE (k:Korrektor {name: row.Korrigiert_von})
    WITH u, k, row
    MERGE (u)<-[ko:KORRIGIERT_VON]-(k)
    ON CREATE SET ko.korrektur=row.Korrektur
    WITH u, row WHERE row.Erfasser IS NOT NULL
    MERGE (er:Erfasser {name: row.Erfasser})
    WITH u, er, row
    MERGE (u)<-[ev:ERFASST_VON]-(er)
    ON CREATE SET ev.zugang=row.Zugang
} IN TRANSACTIONS


:auto LOAD CSV WITH HEADERS FROM 'https://git.thm.de/aksz15/data/-/raw/master/f3Urkunden/f3Urkunden.csv' AS row
CALL {
    WITH row
    MATCH (u:Urkunde {id: row.Objektnummer})
    WITH u, row WHERE row.Sachbetreff IS NOT NULL
    MATCH (sa:Sachbetreff)
    WHERE sa.label=row.Sachbetreff
    MERGE (sa)<-[:HAT_SACHBETREFF]-(u)
    WITH u, row WHERE row.Archiv IS NOT NULL
    MATCH (arch:Archiv)
    WHERE arch.label=row.Archiv
    MERGE (u)-[nachw:NACHWEIS_IN]->(arch)
    SET nachw.signatur=row.Signatur
    WITH u, row WHERE row.Region IS NOT NULL
    MATCH (reg:Region)
    WHERE reg.label=row.Region
    MERGE (u)-[:ERSCHLOSSEN_IN]->(reg)
} IN TRANSACTIONS