# Dokumentation des Vorgehens
Die folgende Dokumentation soll darstellen, wie unser Team bei der Wiederherstellung der Urkunden-Datenbank vorgegangen ist und aufgetretene Schwierigkeiten und Erkenntnisse hervorheben.

## Schritt 1: Sichtung der Daten und Vorbereitung der Modellierung
Um ein grundlegendes Verständnis der Daten zu erhalten, haben wir als ersten Schritt alle verfügbaren Datenquellen und Hinweise gesichtet. Dabei gab es verschiedene Ansatzpunkte:
* `f3Urkunden.csv` als hauptsächliche Datenquelle.
* `*-thesaurus.txt` als ergänzende Daten in Textform.
* `ErfassungsmaskenF3.pdf` als Übersicht zur Erstellung der Daten.
* [http://iserv.adwmainz.de/](http://iserv.adwmainz.de/) als Darstellung des Datenbestands.

Dabei konnten wir der csv-Datei zunächst nicht allzu viele stichhaltige Informationen entnehmen, weil diese aufgrund der Vielzahl an Daten eher unübersichtlich ist. Als sinnvollen Datensatz konnte wir die erste Zeile extrahieren, welche die Header und damit alle vorhandenen Datenwerte gelistet. Diese Header haben wir für die spätere Verwendung als Liste vorgehalten. Jede Zeile scheint eine Urkunde gemeinsam mit ihr zugehörigen Daten zu enthalten, sodass der Import die Datei schlicht Zeile für Zeile abarbeiten können sollte.

Die Thesaurus-Dateien warne zunächst ebenfalls nicht sinnvoll zu verwenden, weil zunächst ihr Kontext identifiziert werden musste. Klar war aber, dass diese hierarchische Ordnungen von Regionen, Archiven und Sachbetreffen darstellen, die jeweils durch einen Knoten mit einer Beziehung auf sich selbst (`[:SUB_OF]`) dargestellt werden müssten. Ebenso war klar, dass für einen Import eine Transformation der Daten nach `.csv` nötig ist.

Am sinnvollsten zu verwenden war das PDF der Erfassungsmasken, da es zeigt, dass verschiedene Typen von Entitäten existieren und dabei hilft, die Datenwerte zu diesen zuzuordnen. Dabei sieht man, dass es verschiedene Typen wie **Original**, **Kopial** oder **Reichsregister** gibt, die alle einen grundsätzlich gleichen Satz an Daten haben und sich nur in einigen Eigenschaften unterscheiden.
* Daher haben wir entschieden, diese Daten jeweils als ein Knoten-Label **Urkunde** zusammenzufassen. Wenn eine Urkunde bestimmte Daten nicht besitzt (z.B. besitzt ein Original keine diplomatischen Angaben), so sollen diese zunächst `null` bleiben.
* Bei diesem Schritt mussten wir von Herrn Kuczera einige fachliche Informationen über die Quelldaten einholen - ohne zu verstehen, was genau abgebildet ist, ist es quasi nicht möglich, die inhaltlichen Zusammenhänge der Daten zu verstehen.

Anhand der Daten ist weiterhin ersichtlich, dass Urkunden mit Sachbetreffen, Archiven und Regionen verknüpft sind. Daher muss die CSV-Datei mit den Daten der Thesaurus-Dateien verknüpft werden. Unklar ist die Bedeutung der letzten beiden sehr kleinen Entitäten **Biographie** und **Ortsbetreff**, die wir anhand der Darstellung nicht sinnvoll zuordnen könnnen.

Die Webseite ist insgesamt relativ unübersichtlich zu navigieren, stellt aber auch nur Urkunden dar. Hier ist zu sehen, dass die in der Eingabemaske zugeordneten Entitäten auch bei den Datenbank-Einträgen angezeigt werden.

## Schritt 2: Modellierung als Graph
Als nächsten Schritt haben wir mit [arrows.app] eine Modellierung des zu erzeugenden Graphen vorgenommen. Allgemein haben wir die Modellierung weitgehend mit deutschen Begriffen vorgenommen, weil der Datensatz in Deutsch vorliegt - eine Umwandlung zu englischen Begriffen wäre denkbar. Dabei haben wir zunächst Labels identifiziert, indem wir die Daten einer Urkunde gesucht haben, die entweder eine eigenständige Person darstellen oder ein bei mehreren Urkunden auftretendes Datum sind:
* **Urkunde** als zusammenfassendes Label für Originale, Kopiate, Reichsregister, ...
* **Erfasser** für den Erfasser einer Urkunde, der als Datum `Erfasser` festgehalten ist.
* **Korrektor** für denjenigen, der Korrekturen (vermutlich am Eintrag der Urkunde) vorgenommen hat, abgespeichert als als Datum `Korrigiert_von`.
* **Empfänger** als Empfänger der Urkunde, gespeichert als Datum `Empfänger`.
* **Kanzlist** als der Ersteller eines Vermerks einer Urkunde, gespeichert als `Kanzlist`. Es ist anzumerken, dass dieser eine Property `Amtsbezeichnung` haben muss.
* **Referent** als weitere Person, die an der Vermerk-Erstellung beteiligt ist (hier ist der fachliche Hintergrund nicht ganz klar).
* **Siegel** als das Siegel, welches auf der Urkunde angebracht wurde. Diese wiederholen sich für einen Absender, sodass sie ein eigener Knoten-Typ sind. Eindeutig identifiziert wird jedes Siegel durch `Siegel_PosseNr`.
  * Eine spannende Frage ist, welche Daten bei jeder Benutzung eines Siegels gleich sind. Wir vermuten, dass dies `Siegelfarbe`, `Sekretsiegel_PosseNr` und `Sekretsiegelfarbe` sind.
  * Eventuell muss das Sekretsiegel ebenfalls ein eigener Knoten sein - hier fehlt uns der fachliche Hintergrund zur Beurteilung.
* **Archiv** als Label für die Daten aus `archiv-thesaurus.txt`. Es sind Archive, in denen die Urkunden liegen.
* **Region** als die Regionen, in denen Urkunden erschlossen wurden. Die Daten stammen aus `region-thesaurus.txt`.
* **Sachbetreff** als die sich wiederholenden sachlichen Betreffe von Urkunden. Die Daten stammen aus `sach-thesaurus.txt`.

Damit waren alle für uns zunächst relevanten Knoten/Labels identifiziert. Ergänzend gibt es die Labels **Ortsbetreff** und **Biographie**, die für uns aber deshalb schwer einzuordnen sind, weil wir nicht wissen, wie diese Daten in Bezug zu Urkunden stehen.

Im nächsten Schritt haben wir die Relationen zwischen diesen Labels eingefügt. Die Benennung der Relationen versucht jeweils, den zugedachten Zweck zu beschreiben - eine Verbesserung ist denkbar. Weiterhin ist zu beachten, dass diese teilweise Properties besitzen:
* **ERSTELLT_VERMERK** als Beziehung eines Kanzlisten zur Urkunde.
* **REFERENT_VERMERK** als Beziehung des Referent zur Urkunde.
* **KORRIGIERT_VON** als Beziehung des Korrektors zur Urkunde. Dabei existiert die Property **Korrektur** für das Datum der Korrektur.
* **ERFASST_VON** als Beziehung des Erfassers zur Urkunde. Hier steht die Property **Zugang** für das Datum.
* **HAT_SIEGEL** als Beziehung von Urkunde und Siegel. Hierbei gibt es mehrere Properties, bei denen wir davon ausgehen, dass sie sich bei jeder Anbringung unterscheiden: **Anbringung**, **Schnurfarbe**, **Sekretsiegellage**, **Besiegelung_Anmerkungen**
* **NACHWEIS_IN** als Beziehung einer Urkunde zum Archiv. Dabei gibt es die Property **Signatur**-
* **ERSCHLOSSEN_IN** als Beziehung von Urkunde und Region.
* **HAT_SACHBETREFF** als Beziehung von Urkunde zu Sachbetreff.
* Die rekursiven Strukturen von Archiven, Regionen und Sachbetreffen werden jeweils durch Labels mit dem Namen **SUB_OF_\*** dargestellt (**SUB_OF_A** usw.).

Zuletzt wurden die Properties zugeordnet. Die graphische Darstellung nutzt dabei Properties mit der Schreibweise der CSV-Header - diese werden beim Import auf Neo4j-Konventionen umgestellt. Die Zuordnung von Properties zu Labels basiert auf den Informationen der Eingabemaske.

## Schritt 3: Aufbereitung der Thesaurus-Daten
Bevor der Import der Daten in Neo4j möglich war mussten zunächst die Thesaurus-Dateien aufbereitet werden, indem diese nicht mehr als hierarchisch eingerückte txt-Datei dargestellt werden, sondern als CSV-Datei. Das Zielformat sollte so aussehen, dass es eine Zeile mit dem eigentlichen Datum (`place` genannt) und eine Zeile `sub_of` gibt, welche den übergeordneten Wert angibt (leer, wenn ein Element auf der obersten Ebene liegt).

Dabei wurde die Python-Bibliothek **Pandas** zur Datenverarbeitung genutzt. Die Daten wurden in Python importiert und für jedes Element geprüft, welcher Knoten gerade übergeordnet ist. Auf diese Weise wurde eine mehrdimensionale Liste erstellt, die Knotennamen und übergeordnete Knotennamen für jeden Knoten enthält. Mittels Pandas wurde dann ein Export in CSV-Dateien vorgenommen. Diese werden im GitLab-Repository [https://git.thm.de/jbrg89/urkunden-files](https://git.thm.de/jbrg89/urkunden-files) gehostet, um in Neo4j einfach importiert werden zu können.

## Schritt 4: Erstellen von Cypher-Skripten zum Import in Neo4j
Nachdem alle Daten vorbereitet wurden konnte der Import in Cypher vorgenommen werden. Dieser stellt weitgehend ein Niederschreiben des bereits formulierten Schemas dar, sodass keine allzu ausführlichen Erklärungen vorgenommen werden sollen.

Der Import der Thesaurus-Dateien läuft jeweils so ab, dass ein neuer Knoten für den `place` einer Zeile erstellt wird. Wenn ein `sub_of`-Wert existiert, so wird versucht, diesen mit **MATCH** zu finden und die `SUB_OF*`-Relation zu erstellen. Das funktioniert, weil die CSV-Dateien die korrekte Reihenfolge haben.

Als nächster Schritt wird die eigentliche CSV-Datei der Urkunden durchlaufen und für jede Zeile zunächst der Urkunden-Knoten mit allen Properties erstellt. Danach erfolgt das Erstellen der Knoten wie **Korrektor** oder **Siegel**, die anhand der Daten in der jeweiligen Zeile erstellt werden können (siehe Beschreibung des Schemas). Durch die Nutzung von **MERGE ... ON CREATE SET** wird sichergestellt, dass Knoten wiederverwendet werden, wenn sie in mehreren Zeilen auftauchen. Für jeden Knoten wird zudem die passende Beziehung gesetzt.

Im letzten Schritt wird das Urkunden-CSV erneut durchlaufen und die Felder **Archiv**, **Sachbetreff** und **Region** betrachtet. Für diese wird jeweils mit **MATCH** der bereits existierende Knoten aus den Thesaurus-Imports gesucht und die Beziehung erstellt. So werden beide Datensätze verknüpft.

Als Vorgehen haben wir die Liste der Properties und Knoten, die wir importieren, Stück für Stück erweitert und mit den Zwischenständen Import-Versuche vorgenommen. Bei jedem Import haben wir die neu hinzugekommen Properties, Labels und Relationen untersucht. So konnten wir feststellen, dass teilweise Transformationen zu anderen Datentypen erforderlich waren. Außerdem sind einige Daten aufgefallen, die durch Semikolons getrennte Listen als Wert haben - mit einer Anwendung von `split` und `coalesce` haben wir diese in Neo4j-List-Properties transformiert.
* Zudem sind vereinzelte Datenfelder aufgefallen, die überall `null` sind - der Datensatz scheint hier Unsauberkeiten aufzuweisen.

## Ausblick
Mit den vorgenommenen Imports erhält der Graph bereits eine erste sinnvolle Form, mit der zur bestehenden Webseite vergleichbare Aufrufe möglich sind. Es gibt allerdings noch einzelne Schritte, die weitergehend bearbeitet werden könnten:
* Erstellung eigener Labels für Urkunden-Typen wie Kopial, Reichsregister, Original, ... - hier muss geklärt werden, ob die abgefragten Use-Cases eine solche Unterscheidung benötigen.
* Übersetzung von Properties in Knoten: Es gibt vereinzelte Properties, bei denen wir festgestellt haben, dass sich ihre Werte häufig wiederholen - hier müsste geklärt werden, inwiefern diese Daten sinnvoll als eigenes Label verwendet werden können.
* Biographie und Ortsbetreff: Da wir für die Daten keine sinnvolle inhaltliche Verknüpfung feststellen konnten haben wir diese zunächst nicht in den Graphen aufgenommen. Sobald die inhaltliche Beziehung klar wird sollten diese ebenfalls in den Graph aufgenommen werden.
* Einige Properties enthalten Daten und könnten in passende Datums-Typen von Neo4j transformiert werden.