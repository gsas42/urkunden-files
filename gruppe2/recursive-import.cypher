:begin
LOAD CSV WITH HEADERS FROM 'https://git.thm.de/jbrg89/urkunden-files/-/raw/master/regional-thesaurus.csv' as row FIELDTERMINATOR ';'
WITH row
MERGE (r:Region {label: row.place})
WITH row, r
WHERE row.sub_of IS NOT null
MATCH (r1:Region {label: row.sub_of})
MERGE (r)-[:SUB_OF_R]->(r1)
:commit

:begin
LOAD CSV WITH HEADERS FROM 'https://git.thm.de/jbrg89/urkunden-files/-/raw/master/archiv-thesaurus.csv' as row1 FIELDTERMINATOR ';'
WITH row1
MERGE (a:Archiv {label: row1.place})
WITH row1, a
WHERE row1.sub_of IS NOT null
MATCH (a1:Archiv {label: row1.sub_of})
MERGE (a)-[:SUB_OF_A]->(a1)
:commit

:begin
LOAD CSV WITH HEADERS FROM 'https://git.thm.de/jbrg89/urkunden-files/-/raw/master/sach-thesaurus.csv' as row2 FIELDTERMINATOR ';'
WITH row2
MERGE (s:Sachbetreff {label: row2.place})
WITH row2, s
WHERE row2.sub_of IS NOT null
MATCH (s1:Sachbetreff {label: row2.sub_of})
MERGE (s)-[:SUB_OF_S]->(s1)
:commit