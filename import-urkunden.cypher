// Empty the graph
MATCH (n) DETACH DELETE n;

// Create indices
CREATE INDEX IF NOT EXISTS FOR (n:Region) ON (n.label);
CREATE INDEX IF NOT EXISTS FOR (n:Archiv) ON (n.label);
CREATE INDEX IF NOT EXISTS FOR (n:Sachbetreff) ON (n.label);
CREATE INDEX IF NOT EXISTS FOR (n:Urkunde) ON (n.id);
CREATE INDEX IF NOT EXISTS FOR (n:Siegel) ON (n.siegel_PosseNr);
CREATE INDEX IF NOT EXISTS FOR (n:Kanzlist) ON (n.name);
CREATE INDEX IF NOT EXISTS FOR (n:Referent) ON (n.name);
CREATE INDEX IF NOT EXISTS FOR (n:Empfaenger) ON (n.name);
CREATE INDEX IF NOT EXISTS FOR (n:Literatur) ON (n.label);
CREATE INDEX IF NOT EXISTS FOR (n:Person) ON (n.name);

// Import Thesaurus data transformed to CSV files
LOAD CSV WITH HEADERS FROM 'https://git.thm.de/jbrg89/urkunden-files/-/raw/master/regional-thesaurus.csv' as row FIELDTERMINATOR ';'
WITH row
MERGE (r:Region {label: row.place})
WITH row, r
WHERE row.sub_of IS NOT null
MATCH (r1:Region {label: row.sub_of})
MERGE (r)-[:SUB_OF]->(r1);

LOAD CSV WITH HEADERS FROM 'https://git.thm.de/jbrg89/urkunden-files/-/raw/master/archiv-thesaurus.csv' as row1 FIELDTERMINATOR ';'
WITH row1
MERGE (a:Archiv {label: row1.place})
WITH row1, a
WHERE row1.sub_of IS NOT null
MATCH (a1:Archiv {label: row1.sub_of})
MERGE (a)-[:SUB_OF]->(a1);

LOAD CSV WITH HEADERS FROM 'https://git.thm.de/jbrg89/urkunden-files/-/raw/master/sach-thesaurus.csv' as row2 FIELDTERMINATOR ';'
WITH row2
MERGE (s:Sachbetreff {label: row2.place})
WITH row2, s
WHERE row2.sub_of IS NOT null
MATCH (s1:Sachbetreff {label: row2.sub_of})
MERGE (s)-[:SUB_OF]->(s1);

// Separate runs are performed for each node type to filter with WITH x IS NOT NULL

// Create the 'Urkunden' nodes
:auto LOAD CSV WITH HEADERS FROM 'https://git.thm.de/aksz15/data/-/raw/master/f3Urkunden/f3Urkunden.csv' AS row
CALL {
    WITH row
    MERGE (u:Urkunde {id: row.Objektnummer})
    ON CREATE SET
    u.quellenDatum=row.Quellendatum,
    u.sortierDatum=row.SortierDatum,
    u.urkundenNummer=toInteger(row.UrkundenNummer),
    u.uNummer=row.UNummer,
    u.kVr=row.KVr,
    u.kVv=row.KVv,
    u.kVRPosition=row.KVRPosition,
    u.kVVPosition=row.KVVPosition,
    u.beschreibstoff=row.Beschreibstoff,
    u.diplomatische_Form=row.Diplomatische_Form,
    u.chmelbeleg=row.Chmelbeleg,
    u.chmelNr=row.ChmelNr,
    u.chmelRegg=row.ChmelRegg,
    u.chmelText=row.ChmelText,
    u.taxreg=split(row.Taxreg, ";"),
    u.rFIII=split(row.RFIII, ";"),
    u.foto=row.Foto,
    u.pön=row.Pön,
    u.wasserzeichen=row.Wasserzeichen,
    u.nachtragsfelder=row.Nachtragsfelder,
    u.bemerkungen=split(row.Bemerkungen, ";"),
    u.sonstiges=row.Sonstiges,
    u.urkNummer=toInteger(row.UrkNummer),
    u.rfNr=row.RfNr,
    u.ergibt_sich_aus=split(row.Ergibt_sich_aus, ";"),
    u.indexDatum=row.IndexDatum,
    u.sF=toInteger(row.SF),
    u.editionsDatum=row.EditionsDatum,
    u.kurzregest=row.Kurzregest,
    u.reggF=row.ReggF,
    u.weitere_Nachweise=split(row.Weitere_Nachweise, ";"),
    u.mehrfachüberlieferung_von=row.Mehrfachüberlieferung_von,
    u.rR=row.RR,
    u.orte=split(row.Orte, ";"),
    u.urkunde=row.Urkunde,
    u.objektart=row.Objektart,
    u.angaben_zum_Original=row.Angaben_zum_Original,
    u.anmerkungen_zur_Kopie=row.Anmerkungen_zur_Kopie,
    u.art_der_Kopie=row.Art_der_Kopie,
    u.datierungszeile_der_Kopie=row.Datierungszeile_der_Kopie,
    u.datum_der_Kopie=row.Datum_der_Kopie,
    u.ausstellungsort=row.Ausstellungsort,
    u.besiegelung_der_Kopie=row.Besiegelung_der_Kopie,
    u.iDNummer=row.IDNummer,
    u.kopie=row.Kopie,
    u.kopieStoff=row.KopieStoff,
    u.kV_Anmerkungen=row.KV_Anmerkungen,
    u.kVrecto=row.KVrecto,
    u.maske=row.Maske,
    u.reggFApparat=row.ReggFApparat,
    u.reggFKV=row.ReggFKV,
    u.reggFText=row.ReggFText,
    u.reggFÜberlieferung=row.ReggFÜberlieferung,
    u.registerText=row.RegisterText,
    u.registrator=row.Registrator,
    u.registraturvermerk=row.Registraturvermerk,
    u.rF=row.RF,
    u.weitere_Überlieferung=row.Weitere_Überlieferung,
    u.erfasser=row.Erfasser,
    u.zugang=u.Zugang,
    u.korrigiert_von=row.Korrigiert_von
    // TODO: create list, transform to dates
    WITH u, split(row.Korrektur, ';') AS korrekturen
    UNWIND korrekturen AS korrektur
    WITH u, [item in split(korrektur, '.') | toInteger(item)] AS dateComponents
    WITH u, collect(date({day: dateComponents[0], month: dateComponents[1], year: dateComponents[2]})) AS korrekturen
    SET u.korrekturen=korrekturen
} IN TRANSACTIONS;

// Create the 'Siegel' nodes and the relationship to 'Urkunden'
:auto LOAD CSV WITH HEADERS FROM 'https://git.thm.de/aksz15/data/-/raw/master/f3Urkunden/f3Urkunden.csv' AS row
CALL {
    WITH row
    MATCH (u:Urkunde {id: row.Objektnummer})
    WITH row, u WHERE row.Siegel_PosseNr IS NOT NULL
    MERGE (s:Siegel {siegel_PosseNr: row.Siegel_PosseNr})
    ON CREATE SET
    u.sekretsiegel_PosseNr=row.Sekretsiegel_PosseNr,
    u.sekretsiegelfarbe=row.Sekretsiegelfarbe,
    u.siegelfarbe=row.Siegelfarbe
    WITH u, s, row
    MERGE (u)-[h:HAT_SIEGEL]->(s)
    ON CREATE SET
    h.anbringung=row.Anbringung,
    h.schnurfarbe=row.Schnurfarbe,
    h.sekretsiegellage=row.Sekretsiegellage,
    h.besiegelung_Anmerkungen=row.Besiegelung_Anmerkungen
} IN TRANSACTIONS;

// Create the 'Kanzlist' nodes and the relationship to 'Urkunden'
:auto LOAD CSV WITH HEADERS FROM 'https://git.thm.de/aksz15/data/-/raw/master/f3Urkunden/f3Urkunden.csv' AS row
CALL {
    WITH row
    MATCH (u:Urkunde {id: row.Objektnummer})
    WITH u, row WHERE row.Kanzlist IS NOT NULL
    MERGE (k:Kanzlist {name: row.Kanzlist})
    SET k.amtsbezeichnung=row.Amtsbezeichnung
    WITH k, u, row
    MERGE (u)<-[:ERSTELLT_VERMERK]-(k)
} IN TRANSACTIONS;

// Create the 'Referent' nodes and the relationship to 'Urkunden'
:auto LOAD CSV WITH HEADERS FROM 'https://git.thm.de/aksz15/data/-/raw/master/f3Urkunden/f3Urkunden.csv' AS row
CALL {
    WITH row
    MATCH (u:Urkunde {id: row.Objektnummer})
    WITH u, row WHERE row.Referent IS NOT NULL
    MERGE (r:Referent {name: row.Referent})
    WITH r, u, row
    MERGE (u)<-[:REFERENT_VERMERKT]-(r)
} IN TRANSACTIONS;

// Create the 'Empfaenger' nodes and the relationship to 'Urkunden'
:auto LOAD CSV WITH HEADERS FROM 'https://git.thm.de/aksz15/data/-/raw/master/f3Urkunden/f3Urkunden.csv' AS row
CALL {
    WITH row
    MATCH (u:Urkunde {id: row.Objektnummer})
    WITH u, row WHERE row.Empfaenger IS NOT NULL
    MERGE (em:Empfaenger {name: row.Empfänger})
    WITH u, em, row
    MERGE (u)-[:EMPFÄNGT]->(em)
} IN TRANSACTIONS;

// Connect the 'Urkunden' nodes with the 'Sachbetreff' nodes from the Thesaurus data
:auto LOAD CSV WITH HEADERS FROM 'https://git.thm.de/aksz15/data/-/raw/master/f3Urkunden/f3Urkunden.csv' AS row
CALL {
    WITH row
    MATCH (u:Urkunde {id: row.Objektnummer})
    WITH u, row WHERE row.Sachbetreff IS NOT NULL
    MATCH (sa:Sachbetreff)
    WHERE sa.label=row.Sachbetreff
    MERGE (sa)<-[:HAT_SACHBETREFF]-(u)
} IN TRANSACTIONS;

// Connect the 'Urkunden' nodes with the 'Archiv' nodes from the Thesaurus data
:auto LOAD CSV WITH HEADERS FROM 'https://git.thm.de/aksz15/data/-/raw/master/f3Urkunden/f3Urkunden.csv' AS row
CALL {
    WITH row
    MATCH (u:Urkunde {id: row.Objektnummer})
    WITH u, row WHERE row.Archiv IS NOT NULL
    MATCH (arch:Archiv)
    WHERE arch.label=row.Archiv
    MERGE (u)-[nachw:NACHWEIS_IN]->(arch)
    SET nachw.signatur=row.Signatur
} IN TRANSACTIONS;

// Connect the 'Urkunden' nodes with the 'Region' nodes from the Thesaurus data
:auto LOAD CSV WITH HEADERS FROM 'https://git.thm.de/aksz15/data/-/raw/master/f3Urkunden/f3Urkunden.csv' AS row
CALL {
    WITH row
    MATCH (u:Urkunde {id: row.Objektnummer})
    WITH u, row WHERE row.Region IS NOT NULL
    MATCH (reg:Region)
    WHERE reg.label=row.Region
    MERGE (u)-[:ERSCHLOSSEN_IN]->(reg)
} IN TRANSACTIONS;

// Create the 'Literature' nodes and all relationships for 'Druckbeleg', 'Literatur', 'RegestBeleg' and 'Erwähnung'
:auto LOAD CSV WITH HEADERS FROM 'https://git.thm.de/aksz15/data/-/raw/master/f3Urkunden/f3Urkunden.csv' AS row
CALL {
    WITH row
    MATCH (u:Urkunde {id: row.Objektnummer})
    WITH u, row WHERE row.Druckbeleg IS NOT NULL
    // Unwind the list using split and UNWIND
    WITH u, split(row.Druckbeleg, ';') AS druckbelegListe
    UNWIND druckbelegListe AS druckbeleg
    // Split the 'druckbeleg' value into label and reference
    WITH u, split(druckbeleg, " / ") AS druckbelegSplit
    // Create the node
    MERGE (l:Literatur {label: druckbelegSplit[0]})
    // Create the relationship
    WITH u, l, druckbelegSplit
    MERGE (u)-[d:DRUCKBELEG]->(l)
    ON CREATE SET d.referenz=druckbelegSplit[1]
} IN TRANSACTIONS;

:auto LOAD CSV WITH HEADERS FROM 'https://git.thm.de/aksz15/data/-/raw/master/f3Urkunden/f3Urkunden.csv' AS row
CALL {
    WITH row
    MATCH (u:Urkunde {id: row.Objektnummer})
    WITH u, row WHERE row.Literatur IS NOT NULL
    // Unwind the list using split and UNWIND
    WITH u, split(row.Literatur, ';') AS literaturListe
    UNWIND literaturListe AS literatur
    // Split the 'literatur' value into label and reference
    WITH u, split(literatur, " / ") AS literaturSplit
    // Create the node
    MERGE (l:Literatur {label: literaturSplit[0]})
    // Create the relationship
    WITH u, l, literaturSplit
    MERGE (u)-[r:LITERATURANGABE]->(l)
    ON CREATE SET r.referenz=literaturSplit[1]
} IN TRANSACTIONS;

:auto LOAD CSV WITH HEADERS FROM 'https://git.thm.de/aksz15/data/-/raw/master/f3Urkunden/f3Urkunden.csv' AS row
CALL {
    WITH row
    MATCH (u:Urkunde {id: row.Objektnummer})
    WITH u, row WHERE row.Regestbeleg IS NOT NULL
    // Unwind the list using split and UNWIND
    WITH u, split(row.Regestbeleg, ';') AS regestBelegListe
    UNWIND regestBelegListe AS regestBeleg
    // Split the 'regestBeleg' value into label and reference
    WITH u, split(regestBeleg, " / ") AS regestBelegSplit
    // Create the node
    MERGE (l:Literatur {label: regestBelegSplit[0]})
    // Create the relationship
    WITH u, l, regestBelegSplit
    MERGE (u)-[r:REGESTBELEG]->(l)
    ON CREATE SET r.referenz=regestBelegSplit[1]
} IN TRANSACTIONS;

:auto LOAD CSV WITH HEADERS FROM 'https://git.thm.de/aksz15/data/-/raw/master/f3Urkunden/f3Urkunden.csv' AS row
CALL {
    WITH row
    MATCH (u:Urkunde {id: row.Objektnummer})
    WITH u, row WHERE row.Erwähnung IS NOT NULL
    // Unwind the list using split and UNWIND
    WITH u, split(row.Erwähnung, ';') AS erwaehnungListe
    UNWIND erwaehnungListe AS erwaehnung
    // Split the 'erwaehnung' value into label and reference
    WITH u, split(erwaehnung, " / ") AS erwaehnungSplit
    // Create the node
    MERGE (l:Literatur {label: erwaehnungSplit[0]})
    // Create the relationship
    WITH u, l, erwaehnungSplit
    MERGE (u)-[r:ERWAEHNUNG]->(l)
    ON CREATE SET r.referenz=erwaehnungSplit[1]
} IN TRANSACTIONS;

// Create the 'Person' nodes by unwinding the 'Personen' property
:auto LOAD CSV WITH HEADERS FROM 'https://git.thm.de/aksz15/data/-/raw/master/f3Urkunden/f3Urkunden.csv' AS row
CALL {
    WITH row
    MATCH (u:Urkunde {id: row.Objektnummer})
    WITH u, row WHERE row.Personen IS NOT NULL
    // Unwind the list using split and UNWIND
    WITH u, split(row.Personen, ';') AS personenListe
    UNWIND personenListe AS person
    // Create the node
    MERGE (p:Person {name: person})
    // Create the relationship
    WITH u, p
    MERGE (u)-[:ERWAEHNT]->(p)
} IN TRANSACTIONS;